# PLWM WEB

## Install Dependencies

### Composer
- **[Download Composer](https://getcomposer.org/)**

### XAMPP
- **[Download XAMPP](https://www.apachefriends.org/index.html)**

### Node
- **[Download Node](https://nodejs.org/en/)**
- after installing need to restart 

### Git Bash
- **[Download Git Bash](https://git-scm.com/downloads)**

### Run Project locally
- git clone https://portfolio101@bitbucket.org/portfolio101/plwm_web.git
- cd to "project_directory"
- Create .env and Copy .env.example
- run "composer install"
- run "npm install"
- run "php artisan key:generate"
- run "php artisan migrate:fresh --seed"
- run "php artisan serve" to run in localhost

### Sample Users
- admin@admin.com
- secret
